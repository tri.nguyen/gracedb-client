# ligo-gracedb
Client software for the <b>Gra</b>vitational-wave <b>C</b>andidate <b>E</b>vent <b>D</b>ata<b>b</b>ase, a web service that organizes candidate events from gravitational wave searches and provides an environment to record information about follow-ups.

For more information, see the [full documentation](https://gw.readthedocs.io/ligo-gracedb/).

## Quick install
```python
pip install ligo-gracedb
```

## Contributing
Please fork this [repository](https://git.ligo.org/lscsoft/gracedb-client) and submit a merge request if you wish to contribute to this package.

The package's unit tests are run as a part of the repository's CI setup.
However, if you want to run the tests manually, see the [testing](ligo/gracedb/test/TESTING.md) documentation.
