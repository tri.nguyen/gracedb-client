API
===

This section provides detailed information about the :py:class:`ligo.gracedb.rest.GraceDb` client class which is the primary method for accessing the GraceDB API.


.. autoclass:: ligo.gracedb.rest.GraceDb
   :members:
   :inherited-members:
   :no-undoc-members:
   :exclude-members: eels, writeEel
   

A separate :class:`GraceDbBasic` class had been provided in the past for password-based authentication, but that functionality is now included in the :class:`GraceDb` class.
:py:class:`ligo.gracedb.rest.GraceDbBasic` is still provided for legacy compatibility, but it is now an exact duplicate of the :py:class:`ligo.gracedb.rest.GraceDb` class.
