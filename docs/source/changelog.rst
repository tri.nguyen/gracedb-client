Changelog
=========

2.2.2 (March 8, 2019)
---------------------

- Bugfix for Python 3 compatibility: remove all usage of ``dict.keys()`` and ``dict.values()``
- Bugfix handling of :obj:`bytes` responses in command-line interface
- Improve command-line interface help


2.2.1 (March 1, 2019)
---------------------

- Bugfix default service URL for command-line interface
- Bugfix packaging


2.2.0 (February 22, 2019)
-------------------------

- Full rework of command-line tool
- Add option for handling legacy ``tagname`` kwarg (instead of ``tag_name``) to :py:meth:`ligo.gracedb.rest.GraceDb.writeLog`
- Add optional ``MassGap`` parameter to :py:meth:`ligo.gracedb.rest.GraceDb.createVOEvent`


2.1.2 (February 5, 2019)
------------------------

- Remove ``skymap_image_filename`` from :py:meth:`ligo.gracedb.rest.GraceDb.createVOEvent`
- Add optional kwarg ``max_results`` to :py:meth:`ligo.gracedb.rest.GraceDb.events` and :py:meth:`ligo.gracedb.rest.GraceDb.superevents`
- Improve testing configuration by adding pytest and tox


2.1.1 (December 5, 2018)
------------------------

- Add new optional parameters to :py:meth:`ligo.gracedb.rest.GraceDb.createVOEvent`:

  - ``BNS``: probability that source is a binary neutron star merger
  - ``NSBH``: probability that source is a neutron star - black hole binary merger
  - ``BBH``: probability that the source is a binary black hole merger
  - ``Terrestrial``: probability that the source is a background noise fluctuation or glitch

- Remove one optional parameter from :py:meth:`ligo.gracedb.rest.GraceDb.createVOEvent`: ``vetted``, specifying whether the candidate has undergone basic human vetting
- Minor bugfixes/changes to integration tests


2.1.0 (November 29, 2018)
-------------------------

- Improve error handling for failed attempts to get service information from API
- Consolidate all authentication types under a single client class
- Add unit tests for handling of credentials


2.0.1 (October 3, 2018)
-----------------------

- Bugfix for truthiness checks for numeric arguments
- Remove permissions detail retrieval (no longer available on server)


2.0.0 (September 25, 2018)
--------------------------

- Add comprehensive compatibility for Python 3.4+
- Add functionality for managing superevent signoffs and permissions
- Remove temporary deprecation measure for including VOEvent file text in response
- Add an option for specifying a server API version
- Update unit tests


2.0.0.dev1 (July 25, 2018)
--------------------------

- Add CI configuration
- Update and centralize package version handling
- Remove ``bin/gracedb`` executable, now built by setup.py as an ``entry_point``
- Add client version to request headers
- Implement temporary measure for backwards compatibility - "fake" VOEvent file text in response
- Add superevent categories


2.0.0.dev0 (June 28, 2018)
--------------------------

- Identical to 1.29.dev1 other than packaging changes, but it was decided after the fact that we should bump the major version number


1.29.dev1 (June 21, 2018)
-------------------------

- More new features for creating and managing superevents


1.29.dev0 (May 31, 2018)
------------------------

- New features for creating and managing superevents


1.28 (April 24, 2018)
---------------------

- Various fixes for Python 3


1.27 (February 13, 2018)
------------------------

- Change ``DEFAULT_SERVICE_URL`` used by command-line client to match other instances
- Update organization of full unit test suite


1.26 (December 1, 2017)
-----------------------

- Significant restructuring of unit tests
- Add several unit tests
- Update :py:meth:`ligo.gracedb.rest.GraceDb.createEvent` method to properly handle multiple labels and to work with upcoming server code changes
- Fix whitespace issues


1.25 (August 2, 2017)
---------------------

- Fix packages ``provide``, ``requirements``, and build dependencies
- Add ability to create events with labels to the command-line client
- Add ability to set offline parameter with the command-line client
- Update unit tests


1.24 (May 25, 2017)
-------------------

- Add a check on .netrc file permissions
- Add capability to create events with labels attached
- Add offline boolean parameter when creating events


1.23 (February 3, 2017)
-----------------------

- Improve exception handling and display of tracebacks in REST client
- Catch and handle exceptions in logging handler
- Update of dependencies


1.22 (January 10, 2017)
-----------------------

- Fix bug related to decoding logs
- Improve log handler cleanup
- Remove calls to ``sys.exit()`` from :py:mod:`ligo.gracedb.rest`


1.21 (December 13, 2016)
------------------------

- Modernize Debian packaging and port to Python 2 and 3
- Make logging handler non-blocking


1.20 (February 11, 2016)
------------------------

- Improve error handling for expired or missing credentials
- Improved error handling when server returns non-JSON response
- Added ``--use-basic-auth`` option to command-line client 


1.19.1 (October 22, 2015)
-------------------------

- Force TLSv1 for Python versions less than 2.7.9
- Change ``ligo.gracedb.rest.GraceDb.adjustResponse()`` to put retry-after in the JSON response for 429 respose codes
- Changed test service URL to gracedb-test.ligo.org
- Introduced wait time to test suite


1.19 (July 29, 2015)
--------------------

- Bugfixes: comma separated strings for EM Observations and CLI ``createLog`` call
- Capture of additional kwargs to facilitate HardwareInjection event upload
- Packaging improvements (ligo as namespace package)


1.18 (May 13, 2015)
-------------------

- Add comment to EM Observation record
- Allow Python lists as arguments to writeEMObservation
- Changed ligo to namespace package


1.18.dev0 (April 20, 2015)
--------------------------

- New features for robotic basic auth
- New features for EM observation records


1.17 (March 25, 2015)
---------------------

- Bugfix for Python version incompatibility in 1.16
- New methods/tests for creating and retrieving VOEvents
- Bugfix for gittag test


1.16 (February 11, 2015)
------------------------

- Fixes for glue 1.47 to the command line client
- Use SSLContext and explicitly turn off client-side server verification
- Fix for command that lists groups, pipelines, and searches 
- Allow multiple tags to be added to an event log at creation time
- Updates to unit test data


1.15 (October 31, 2015)
-----------------------

- Features for EMBB Event Log upload and retrieval
- Changes for (Group,Type) -> (Group,Pipeline,Search) transition
- Added docstrings


1.14 (December 18, 2013)
------------------------

- Fixed 10 and 1000 event limits. Corresponds to issues 986 and 787 (on redmine page)


1.13 (June 28, 2013)
--------------------

- Fixed renegotiation regression.  Corresponds to issue 951 (on redmine page)


1.12 (June 27, 2013)
--------------------

- Changed client to use REST API exclusively
- Extended REST API to include all API functionality


1.11 (January 24, 2013)
-----------------------

- Re-add ``--with python2`` to match other ligo-* Python modules
- Fix bug where stdin is specified, it is not actually read
- Add python-ligo-common to dependencies


1.10 (January 18, 2013)
-----------------------

- CLI replace feature had subtle openssl bug; now using REST client
- Typo in help text
- Typo in test.py wrt CWB test data
- Remove ``--with-python2`` from debian/rules


1.9 (January 16, 2013)
----------------------

- Add slot feature to command line interface
- Add unit tests for CLI slot feature
- Add rudimentary test code documentation


1.8 (January 4, 2013)
---------------------

- Incorporate Leo's patch for fixing import problem


1.7 (December 21, 2012)
-----------------------

- Add workaround for Python bug http://bugs.python.org/issue11898
- Add Leo Singer's convenience classes for logging


1.6 (December 19, 2012)
-----------------------

- Fix some typos
- Add more unit tests for GraceDb class
- Add test script for command line client
- Improve unit tests' usability


1.5 (December 12, 2012)
-----------------------

- Source 3.0 format
- Add support for REST API
- Add unit tests and test data


1.4 (July 19, 2012)
-------------------

- Fixed SSL renegotiation bug on large file upload.


1.3 (June 14, 2012)
-------------------

- Replace simplejson with cjson because simplejson is not available everywhere in the LSC


1.2 (June 11, 2012)
-------------------

- Add file download command


1.1 (January 22, 2012)
----------------------

- Bug Fix.


1.0 (November 1, 2011)
----------------------

- Initial release
