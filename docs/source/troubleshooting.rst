Troubleshooting
===============
Running into problems using the client?
This page contains some suggestions and pieces of information that will be useful if you need to contact the developers.


Check package version
---------------------
Is your version of the package horribly out of date?
Check the version and find its release date in the :ref:`changelog:Changelog`.
If it's older than ~1 year, you may want to upgrade and see if that resolves your issue.

There are a variety of ways to check the package version, listed here in order of ease:

- With the command-line tool::

    gracedb --version

- If you have installed the package with pip::

    pip freeze | grep ligo-gracedb

- If you have checked out the git repository::

    python setup.py --version

- If the package is installed as an SL7 system package::

    yum info python*-ligo-gracedb

- If the package is installed as a Debian system package::

    dpkg -l | grep ligo-gracedb


Check your connection to the server
-----------------------------------
Can you connect to the server at all?
If you're using the Python module, instantiate a :py:class:`ligo.gracedb.rest.GraceDb` object as you normally would and do::

    response = client.ping()
    print(response.status)

If you're using the command-line tool, do::

    gracedb ping

Note that you will need to use the ``service_url`` keyword argument to the client constructor, or the ``--service-url`` argument for the command-line tool if you do not want to use the production GraceDB server.

If you don't get an HTTP response with a 200 status code, there is likely a problem with your connection to the server.


Check your credentials
----------------------
Is the client using the credentials that you intend it to use?
If you're using the Python module, instantiate a :py:class:`ligo.gracedb.rest.GraceDb` object as you normally would and do::

    client.show_credentials()

If you're using the command-line tool, do::

    gracedb credentials client

Does the output match what you expect in terms of authentication type and credentials?
If not, check the description of how authentication credentials can be provided to the client class :ref:`here <api:API>`.
For the command-line client, see :ref:`here <cli:Credentials>`.


Check your user account on the server
-------------------------------------
Is the server identifying you with the user account that you expect it to?
If you're using the Python module, instantiate a :py:class:`ligo.gracedb.rest.GraceDb` object as you normally would and do::

    client.get_user_info()

If you're using the command-line tool, do::

    gracedb credentials server

Note that you will need to use the ``service_url`` keyword argument to the client constructor, or the ``--service-url`` argument for the command-line tool if you do not want to use the production GraceDB server.

Does the output match the user account you would expect to use on the server?
If it does not, and you have confirmed that the client is using the correct credentials and authentication type in the previous step, then there may be a server-side issue.


Still having problems?
----------------------
If you are still having problems, try to find a more experienced user who can help you sort out your issue.
If that resource is not available to you or you still cannot resolve your issue, LIGO-Virgo users can contact the developers directly, or send an email to the uwm-help ticketing queue or the DASWG mailing list.


Bug reporting
-------------
Did you find a bug?
Please `create an issue <https://git.ligo.org/lscsoft/gracedb-client/issues>`__ on the project page and include as much information as possible.
Some examples of useful information are:

- Operating system
- Client version
- An error traceback
- What you were doing when the error occurred
- What you expected the result to be
- Specific steps to reproduce the issue
