User guide
==========
This section provides information about using the client class.
Detailed information on the API is available on the :ref:`api:API` page, and on the command-line interface at :ref:`cli:Command-line interface`.


Creating a client instance
--------------------------
:py:class:`ligo.gracedb.rest.GraceDb` is the client class and is used for all API interactions.
To instantiate a client object, do::

    from ligo.gracedb.rest import GraceDb

    client = GraceDb()

By default, the client uses the API of the production GraceDB server, https://gracedb.ligo.org/api/.
To specify another server, use the ``service_url`` keyword argument::

    client = GraceDb(service_url='https://gracedb-playground.ligo.org/api/')

If you are working behind a proxy, use the ``proxy_host`` and ``proxy_port`` arguments to specify the parameter of the proxy server::

    client = GraceDb(proxy_host='myproxy.com', proxy_port=8443)

More information about the client class and the options which can be provided to the constructor is covered in detail on the :ref:`api:API` page.


Response contents
-----------------
Most of the client methods return a :class:`httplib.HTTPResponse` object.
The server API provides a JSON response which is loaded into a Python ``dict`` by using the ``json()`` method::

    response = client.event('G1234')
    data = response.json()

The exact structure of the resulting dictionary varies depending on which resource type you are retrieving (event, superevent, log entry, etc.).
A variety of examples are available in the server code `documentation <https://gracedb.ligo.org/documentation/models.html>`__.

Note that most client methods will raise a :py:class:`ligo.gracedb.exceptions.HTTPError` if the response code indicates an error (>= 400).


Basic usage for events
----------------------

Create an event
~~~~~~~~~~~~~~~
Create an event on the server with the :py:meth:`ligo.gracedb.rest.GraceDb.createEvent` method::

    response = client.createEvent('CBC', 'gstlal', '/path/to/event/data/file.xml', search='LowMass')

Any LVC user can create "test" events by specifying the first argument (analysis group) as ``'Test'``.
Special permissions are required to upload non-test events.
You can get a list of available analysis groups with :py:attr:`ligo.gracedb.rest.GraceDb.groups`, available analysis pipelines with :py:attr:`ligo.gracedb.rest.GraceDb.pipelines`, or a list of searches with :py:attr:`ligo.gracedb.rest.GraceDb.searches`.

The type of data file depends on which pipeline is submitting the event.
If you are not a pipeline expert, you may want to download a file from an existing event and use that for creating test events.


Update an event
~~~~~~~~~~~~~~~
Update an event's parameters by uploading a new event data file with the :py:meth:`ligo.gracedb.rest.GraceDb.replaceEvent` method::

    response = client.replaceEvent('G123456', '/path/to/new/event/data/file.xml')

Only the user who originally submitted the event is allowed to update it.


Search for events
~~~~~~~~~~~~~~~~~
Use the :py:meth:`ligo.gracedb.rest.GraceDb.events` method to search for events and get an iterator::

    event_iterator = client.events('far < 1e-10')
    graceids = [event['graceid'] for event in event_iterator]

See the server `documentation <https://gracedb.ligo.org/documentation/queries.html>`__ for help with constructing search queries.
Note that test and MDC events are not included in the search results by default.


Retrieve an individual event
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The :py:meth:`ligo.gracedb.rest.GraceDb.event` method is used to get information about an individual event::

    response = client.event('G1234')


Basic usage for superevents
---------------------------

Create a superevent
~~~~~~~~~~~~~~~~~~~
Use the :py:meth:`ligo.gracedb.rest.GraceDb.createSuperevent` method to create a superevent::

    response = client.createSuperevent(1238350677, 1238350680, 1238350682, 'G1234')

The event ``G1234`` must already exist on the server, and the ``category`` (production, test, or MDC) of the superevent must match that of the event.
Special permissions are required to create non-test superevents.


Update a superevent
~~~~~~~~~~~~~~~~~~~
Use the :py:meth:`ligo.gracedb.rest.GraceDb.updateSuperevent` method to update a superevent::

    response = client.updateSuperevent('S190202p', t_0=1238350700, preferred_event='G2345')

Note that this method is used to update the ``t_start``, ``t_0``, ``t_end``, and ``preferred_event`` parameters of the superevent only.
To add events to or remove events from a superevent, use :py:meth:`ligo.gracedb.rest.GraceDb.addEventToSuperevent` or :py:meth:`ligo.gracedb.rest.GraceDb.removeEventFromSuperevent` methods.


Search for superevents
~~~~~~~~~~~~~~~~~~~~~~
Use the :py:meth:`ligo.gracedb.rest.GraceDb.superevents` method to search for superevents and get an iterator::

    superevent_iterator = client.superevents('public is_gw: True')
    superevent_ids = [superevent['superevent_id'] for superevent in superevent_iterator]

See the server `documentation <https://gracedb.ligo.org/documentation/queries.html>`__ for help with constructing search queries.
Note that test and MDC superevents are not included in the search results by default.


Retrieve an individual superevent
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The :py:meth:`ligo.gracedb.rest.GraceDb.superevent` method is used to get information about an individual superevent::

    response = client.superevent('S180331d')


Annotations
-----------
Both events and superevents can be annotated with log entries, file uploads, labels, and more.
There are several methods attached to the client class for creating these annotations and for getting information about existing annotations.
Most of these methods work for both events and superevents and automatically determine which one you are interested in based on the format of the ID provided.


Log entries
~~~~~~~~~~~
To create a log entry, use the :py:meth:`ligo.gracedb.rest.GraceDb.writeLog` method::

    response = client.writeLog('G1234', 'comment on event')

You can also upload a file with a log entry::

    response = client.writeLog('S190202b', 'comment on event', filename='/path/to/file/')

To get a log or list of logs attached to an event or superevent, use the :py:meth:`ligo.gracedb.rest.GraceDb.logs` method::

    response = client.logs('G1234', 12)

Here the second argument is the index number of the log, which represents its chronological order in the entire set of logs for the event or superevent.
If this argument is not provided, the entire set of logs for the event or superevent will be retrieved.


Files
~~~~~
The procedure for uploading files is covered in the previous section on log entries.
To retrieve a list of files attached to an event or superevent, use :py:meth:`ligo.gracedb.rest.GraceDb.files`::

    response = client.files('G1234')

If a specific filename is provided, the server will provide the contents of the file with that name that is associated with the given event or superevent::

    response = client.files('G1234', 'skymap.fits.gz')
    file_contents = response.read()

Note that files are the server are **versioned**; i.e., uploading a file with the same name as another file attached to the same event or superevent will generate a new version.
Different versions are specified by appending ``,N`` to the filename, where ``N`` is the version number.
Filenames without a version always point to the latest version of the file.


Labels
~~~~~~
Labels can be applied to or removed from events or superevents.
They are generally used to indicate some sort of state.
Examples:

- ``ADVREQ``: advocate signoff is requested
- ``PE_READY``: parameter estimation results are available
- ``DQV``: the event or superevent is vetoed due to poor data quality

New labels cannot be created by users.
To see which labels are available on the server, use :py:attr:`ligo.gracedb.rest.GraceDb.allowed_labels`.
A list of labels with descriptions is provided `here <https://gracedb.ligo.org/documentation/labels.html>`__.

To add a label, use :py:meth:`ligo.gracedb.rest.GraceDb.writeLabel`::

    response = client.writeLabel('G1234', 'ADVREQ')

To remove a label, use :py:meth:`ligo.gracedb.rest.GraceDb.removeLabel`::

    response = client.removeLabel('S190214g', 'INJ')


Tags
~~~~
Tags can be added to log entries to indicate the topic or process which the log entry is related to.
For example, the ``data_quality`` tag can be used to indicate file uploads or comments which are related to data quality studies.
Tags can also be created by users - if the tag you specify does not presently exist on the server, it will be created.

To add a tag to a log entry, use the :py:meth:`ligo.gracedb.rest.GraceDb.addTag` method and specify the event or superevent, the log entry ID number, and the tag name::

    response = client.addTag('G1234', 12, 'em_follow')

To remove a tag from a log entry, use :py:meth:`ligo.gracedb.rest.GraceDb.removeTag`::

    response = client.removeTag('S190301a', 16, 'pe')

There are a few tags which are used for exposing information (``lvem``, ``public``) to non-internal users.
Special permissions are required to apply or remove these tags.


Other information
-----------------
Additional methods are provided for managing signoffs, uploading EM observation data, and more.
These are not covered in detail here because they are rather specialized options.
See :ref:`api:API` for information about the client methods used for these actions.
